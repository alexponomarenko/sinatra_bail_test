# frozen_string_literal: true

# Sinatra App for Bail testing purpose
class Application < Sinatra::Base
  configure :development do
    register Sinatra::Reloader
  end

  get '/' do
    "Bail version: #{Bail::VERSION}"
  end
end
